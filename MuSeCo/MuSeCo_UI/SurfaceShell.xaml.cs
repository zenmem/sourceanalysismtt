using System;
using Microsoft.Surface;
using Microsoft.Surface.Presentation.Controls;
using MuSeCo_ViewModel.Gesture;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Surface.Presentation;
using System.IO;
using System.Resources;
using System.Globalization;
using System.Collections;
using Microsoft.Surface.Presentation.Input;
using System.Windows.Media.Animation;
using MuSeCo_ViewModel;
using System.Collections.ObjectModel;
using MuSeCo_Model;

namespace MuSeCo_UI
{

   
    /// <summary>
    /// Interaction logic for SurfaceShell.xaml
    /// </summary>
    public partial class SurfaceShell : SurfaceWindow
    {

        private SurfaceShellViewModel vm;



        #region ctor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SurfaceShell()
        {
            InitializeComponent();

            //vm = new SurfaceShellViewModel();
            vm = new SurfaceShellViewModel(this.testview);

            this.DataContext = vm;
           
            // Add handlers for window availability events
            AddWindowAvailabilityHandlers();

            

        }
        #endregion

  


      

        #region OnClosed
        /// <summary>
        /// Occurs when the window is about to close. 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            // Remove handlers for window availability events
            RemoveWindowAvailabilityHandlers();
        }
        #endregion

        #region Surface handlers
        /// <summary>
        /// Adds handlers for window availability events.
        /// </summary>
        private void AddWindowAvailabilityHandlers()
        {
            // Subscribe to surface window availability events
            ApplicationServices.WindowInteractive += OnWindowInteractive;
            ApplicationServices.WindowNoninteractive += OnWindowNoninteractive;
            ApplicationServices.WindowUnavailable += OnWindowUnavailable;
        }

        /// <summary>
        /// Removes handlers for window availability events.
        /// </summary>
        private void RemoveWindowAvailabilityHandlers()
        {
            // Unsubscribe from surface window availability events
            ApplicationServices.WindowInteractive -= OnWindowInteractive;
            ApplicationServices.WindowNoninteractive -= OnWindowNoninteractive;
            ApplicationServices.WindowUnavailable -= OnWindowUnavailable;
        }

        /// <summary>
        /// This is called when the user can interact with the application's window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnWindowInteractive(object sender, EventArgs e)
        {
            //TODO: enable audio, animations here
        }

        /// <summary>
        /// This is called when the user can see but not interact with the application's window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnWindowNoninteractive(object sender, EventArgs e)
        {
            //TODO: Disable audio here if it is enabled

            //TODO: optionally enable animations here
        }

        /// <summary>
        /// This is called when the application's window is not visible or interactive.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnWindowUnavailable(object sender, EventArgs e)
        {
            //TODO: disable audio, animations here
        }
        #endregion
    }
}