using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using Microsoft.Surface.Presentation.Input;
using MuSeCo_ViewModel;

namespace MuSeCo_UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            ServiceLoader.LoadRunTimeServices();
        }
        
    }
}