﻿using System.Windows;
using MuSeCo_Model;
using MuSeCo_ViewModel;

namespace MuSeCo_UI.TemplateSelector
{
    public class ScatterViewDataTemplateSelector : System.Windows.Controls.DataTemplateSelector
    {
        public DataTemplate SearchBarTemplate { get; set; }
        public DataTemplate SearchResultTemplate { get; set; }
        public DataTemplate ItemDataTemplate { get; set; }
        public DataTemplate RadialMenuTemplate { get; set; }
        public DataTemplate SurfaceNoteTemplate { get; set; }
        public DataTemplate ComparePictureTemplate { get; set; }
        public DataTemplate TaktCompareTemplate { get; set; }


        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            DataTemplate retVal = null;

             if (item is SearchResultViewModel)
                retVal = SearchResultTemplate;
            else if (item is RadialMenuViewModel)
                retVal = RadialMenuTemplate;
            else if (item is SurfaceNoteViewModel)
                retVal = SurfaceNoteTemplate;
            else if (item is TaktCompareViewModel)
                retVal = TaktCompareTemplate;
            else if (item is ItemPage)
                retVal = ItemDataTemplate;


            return retVal;
        }
    }
}
