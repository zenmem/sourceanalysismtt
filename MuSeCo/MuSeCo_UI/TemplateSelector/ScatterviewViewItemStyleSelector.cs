﻿using System.Windows;
using System.Windows.Controls;
using MuSeCo_Model;
using MuSeCo_ViewModel;

namespace MuSeCo_UI.TemplateSelector
{
 
    public class ScatterviewViewItemStyleSelector : StyleSelector
    {
        public Style SearchBarScatterViewItemStyle { get; set; }
        public Style SearchResultScatterViewItemStyle { get; set; }
        public Style ItemScatterViewItemStyle { get; set; }

        public override Style SelectStyle(object item, DependencyObject container)
        {
            Style retVal = null;

            
             if (item is SearchResultViewModel)
                retVal = SearchResultScatterViewItemStyle;
            else if (item is ItemPage)
                retVal = ItemScatterViewItemStyle;

            return retVal;
        }
    }
}
