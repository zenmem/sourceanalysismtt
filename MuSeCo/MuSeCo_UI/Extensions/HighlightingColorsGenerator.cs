﻿using System;
using System.Collections.Generic;

namespace MuSeCo_UI.Extensions
{
    internal class HighlightingColorsGenerator
    {
        #region Fields

        private static double _golden_ratio_conjugate = 0.618033988749895;

        private static double h = 0;

        private static Random rnd = new Random();

        #endregion Fields

        #region Constructors

        private HighlightingColorsGenerator()
        {
        }

        #endregion Constructors

        #region Methods

        public static HslBrushExtension generateColor(double s, double l, double a)
        {
            h += _golden_ratio_conjugate;
            h %= 1;
            HslBrushExtension brush = new HslBrushExtension();
            brush.H = h * 360;
            brush.S = s;
            brush.L = l;
            brush.A = a;
            return brush;
        }

        public static List<HslBrushExtension> generateColorSet(int n, double s, double l, double a)
        {
            List<HslBrushExtension> brushes = new List<HslBrushExtension>();

            for (int i = 0; i < n; i++)
                brushes.Add(generateColor(s, l, a));

            return brushes;
        }

        public static void resetHueToRandom()
        {
            h = rnd.NextDouble();
        }

        #endregion Methods
    }
}