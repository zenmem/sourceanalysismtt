﻿using System;
using System.Configuration;

namespace MuSeCo_UI
{
    internal sealed class ConfigManager
    {
        public static bool KioskMode
        {
            get
            {
                try
                {
                    return Convert.ToBoolean(ConfigurationManager.AppSettings["KioskMode"]);
                }
                catch
                {
                    return false;
                }
            }
        }
    }
}
