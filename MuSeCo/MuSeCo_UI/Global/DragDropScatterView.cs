﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using SoftwareLab.Sys;
using Microsoft.Practices.Prism.Events;
using MuSeCo_ViewModel;
using MuSeCo_Model;
using System.Collections.Specialized;

namespace MuSeCo_UI
{
    /// <summary>
    /// Eine Klasse, die die ScatterView mit Drag & Drop erweitert
    /// </summary>
    public class DragDropScatterView : ScatterView
    {
        public DragDropScatterView()
        {
            Background = Brushes.Transparent;
            AllowDrop = true;

            Loaded += new RoutedEventHandler(OnLoaded);
            Unloaded += new RoutedEventHandler(OnUnloaded);
        }

        private void OnItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        { }

        #region Public Properties

        /// <summary>
        /// Eigenschaft, um zu identifizieren, ob das ScatterViewItem gezogen werden kann.
        /// </summary>
        public static readonly DependencyProperty AllowDragProperty = DependencyProperty.Register(
            "AllowDrag",
            typeof(bool),
            typeof(DragDropScatterView),
            new PropertyMetadata(true/*defaultValue*/));

        /// <summary>
        /// Getter für die AllowDrag Eigenschaft.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static bool GetAllowDrag(DependencyObject element)
        {
            if (!(element is ScatterViewItem))
            {
                throw new InvalidOperationException("AllowDragOnlyOnScatterViewItem");
            }

            return (bool)element.GetValue(AllowDragProperty);
        }

        /// <summary>
        /// Setter für die AllowDrag Eigenschaft.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetAllowDrag(DependencyObject element, bool value)
        {
            if (!(element is ScatterViewItem))
            {
                throw new InvalidOperationException("AllowDragOnlyOnScatterViewItem");
            }

            element.SetValue(AllowDragProperty, value);
        }

        #endregion

        #region Private Methods

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            SurfaceDragDrop.AddDropHandler(this, OnCursorDrop);
            AddHandler(ScatterViewItem.ContainerManipulationStartedEvent, new ContainerManipulationStartedEventHandler(OnManipulationStarted));
        }

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            SurfaceDragDrop.RemoveDropHandler(this, OnCursorDrop);
            RemoveHandler(ScatterViewItem.ContainerManipulationStartedEvent, new ContainerManipulationStartedEventHandler(OnManipulationStarted));
        }

        private void OnManipulationStarted(object sender, RoutedEventArgs args)
        {

            ScatterViewItem svi = args.OriginalSource as ScatterViewItem;
            if (svi != null && DragDropScatterView.GetAllowDrag(svi))
            {
                svi.BeginDragDrop(svi.DataContext);

            }
        }

        private void OnCursorDrop(object sender, SurfaceDragDropEventArgs args)
        {
            SurfaceDragCursor droppingCursor = args.Cursor;

            if (droppingCursor.CurrentTarget == this && droppingCursor.DragSource != this)
            {
                if (!Items.Contains(droppingCursor.Data))
                {
                    // Benachrichtigen den Empfänger, das gedroppte ISearchable-Objekt hinzuzufügen
                    ServiceContainer.Instance.GetService<IEventAggregator>().GetEvent<AddEvent>().Publish(droppingCursor.Data as ISearchable);

                    // übergibt dem SurfaceShellViewModel die Itempages als ScatterViewItem damit die TidyUp-Geste von Fabian Mense ausgeführt werden kann
                    var svi = ItemContainerGenerator.ContainerFromItem(droppingCursor.Data) as ScatterViewItem;
                    ServiceContainer.Instance.GetService<IEventAggregator>().GetEvent<AddScatterItemEvent>().Publish(svi);

                    if (svi != null)
                    {

                        svi.Center = droppingCursor.GetPosition(this);
                        svi.Orientation = 0;
                    }
                    
                }
            }
       
        }

        #endregion
    }
}
