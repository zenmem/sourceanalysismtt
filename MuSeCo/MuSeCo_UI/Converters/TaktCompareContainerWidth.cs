﻿using MuSeCo_Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MuSeCo_UI.Converters
{
    class TaktCompareContainerWidth : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            IEnumerable<Item> items = (IEnumerable<Item>)values[0];
            int pageNum = (int)values[1];
            double viewBoxHeight = (double)values[2];
            double placeHolderWidth = (double)values[3];
            double genWidth = 0;
            foreach (Item item in items)
            {
                if (pageNum < 0)
                    pageNum = 0;
                if (pageNum >= item.Pages.Count)
                    pageNum = item.Pages.Count - 1;

                double imgWidth = item.Pages.ElementAt(pageNum).Width;
                double imgHeight = item.Pages.ElementAt(pageNum).Height;
                double factor = 0;
                if (imgHeight != 0)
                {
                    factor = imgWidth / imgHeight;
                }
                genWidth += factor * viewBoxHeight //current width according to the scale factor
                    + 5 //margin (right)
                    + 2 * 2;// border (left & right);
            }
           genWidth += placeHolderWidth;
            return genWidth;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
