﻿using MuSeCo_Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace MuSeCo_UI.Converters
{
    class ItemPageSelector : IMultiValueConverter

    {
        public object Convert(object[] values, Type targetType,
           object parameter, CultureInfo culture)
        {
            if(values[0] is Item && values[0] != null)
            {
                int pageNum = (int)values[1];
                Item item = (Item)values[0];
                if (pageNum < item.Pages.Count && pageNum >= 0)
                {
                    return new BitmapImage(new Uri(@item.Pages.ElementAt(pageNum).Url));
                }
                else
                {
                    if(pageNum < 0)
                    {
                        return new BitmapImage(new Uri(@item.Pages.ElementAt(0).Url));
                    } else if (pageNum >= item.Pages.Count)
                    {
                        return new BitmapImage(new Uri(@item.Pages.ElementAt(item.Pages.Count - 1).Url));
                    } else
                    {
                        return null;
                    }
                }
            } else
            {
                string defaultImageUrl = (string) values[2];
                return defaultImageUrl;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes,
            object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
