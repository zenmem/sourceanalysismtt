﻿using MuSeCo_UI.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MuSeCo_UI.Converters
{
    public class ColorConverter : IMultiValueConverter
    {
        #region Methods

        public object Convert(
          object[] values,
          Type targetType,
          object parameter,
          CultureInfo culture)
        {
            List<HslBrushExtension> brushes = values[0] as List<HslBrushExtension>;
            if (brushes == null) return HighlightingColorsGenerator.generateColor(100, 50, 20).getBrush();

            if (values[1].Equals(DependencyProperty.UnsetValue)) return HighlightingColorsGenerator.generateColor(100, 0, 50).getBrush();
            int index = (int)values[1];
            if (index >= brushes.Count)
                return HighlightingColorsGenerator.generateColor(100, 50, 20).getBrush();
            return brushes[index].getBrush();
        }

        public object[] ConvertBack(
            object value,
            Type[] targetTypes,
            object parameter,
            CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }

        #endregion Methods
    }
}