﻿using Blake.NUI.WPF.Gestures;
using Microsoft.Practices.Prism.Events;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using MuSeCo_Model;
using MuSeCo_UI.Reusable_Classes;
using MuSeCo_UI.View;
using MuSeCo_ViewModel;
using SoftwareLab.Sys;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MuSeCo_UI.View
{
    /// <summary>
    /// Interaktionslogik für TaktCompare.xaml
    /// </summary>
    public partial class TaktCompare : BaseView
    {

        TaktCompareViewModel vm;

        private const int DragThreshold = 15;

        // List to store the input devices those do not need do the dragging check.
        private List<InputDevice> ignoredDeviceList = new List<InputDevice>();

        public TaktCompare()
        {
            InitializeComponent();
            vm = new TaktCompareViewModel();
            DataContext = vm;
            //SurfaceDragDrop.AddPreviewQueryTargetHandler(mainGrid, Container_PreviewQueryTarget);
            Unloaded += new RoutedEventHandler(OnUnloaded);
            //SurfaceDragDrop.AddPreviewQueryTargetHandler(PanelRight, Container_PreviewQueryTarget);
          //  Unloaded += new RoutedEventHandler(OnUnloaded);
            //Events.RegisterGestureEventSupport(this);
            //Events.AddTapGestureHandler(mainGrid, PanelLeft_TapGesture);

        }

        //Verarbeitet DropEvent
        private void StackPanel_Drop(object sender, SurfaceDragDropEventArgs e)
        {
            try
            {
                if(e.Cursor.Data.GetType().Name.Equals("ItemPage"))
                {
                    ItemPage ta = e.Cursor.Data as ItemPage;
                    vm.Items.Add(ta.Parent);
                }
                if (e.Cursor.Data.GetType().Name.Equals("SearchResultViewModel"))
                {
                    SearchResultViewModel item = e.Cursor.Data as SearchResultViewModel;
                    var pages = item.ItemPages;
                    vm.Items.Add(pages.First().Parent);
                }
              
            }
         catch(Exception x)
            {
                Console.Write("" +x);
            }
        }

        private void StartDragDrop(ListBox sourceListBox, InputEventArgs e)
        {
            // Check whether the input device is in the ignore list.
            if (ignoredDeviceList.Contains(e.Device))
            {
                return;
            }

            InputDeviceHelper.InitializeDeviceState(e.Device);

            Vector draggedDelta = InputDeviceHelper.DraggedDelta(e.Device, (UIElement)sourceListBox);

            // If this input device has moved more than Threshold pixels horizontally,
            // put it to the ignore list and never try to start drag-and-drop with it.
            if (Math.Abs(draggedDelta.X) > DragThreshold)
            {
                ignoredDeviceList.Add(e.Device);
                return;
            }

            // If this input device has moved less than Threshold pixels vertically 
            // then this is not a drag-and-drop yet.
            if (Math.Abs(draggedDelta.Y) < DragThreshold)
            {
                return;
            }

            ignoredDeviceList.Add(e.Device);

            // try to start drag-and-drop,
            // verify that the cursor the input device was placed at is a ListBoxItem
            DependencyObject downSource = InputDeviceHelper.GetDragSource(e.Device);
            Debug.Assert(downSource != null);

            SurfaceListBoxItem draggedListBoxItem = GetVisualAncestor<SurfaceListBoxItem>(downSource);
            if (draggedListBoxItem == null)
            {
                return;
            }

            // Get Xml source.
            Item data = draggedListBoxItem.Content as Item;

            // Data should be copied, because the Stack rejects data of the same instance.
            //data = data.Clone() as Item;

            // Create a new ScatterViewItem as cursor visual.
            ScatterViewItem cursorVisual = new ScatterViewItem();
            //cursorVisual.Style = (Style)FindResource("ScatterItemStyle");
            cursorVisual.Content = data;

            IEnumerable<InputDevice> devices = null;

            TouchEventArgs touchEventArgs = e as TouchEventArgs;
            if (touchEventArgs != null)
            {
                devices = MergeInputDevices(draggedListBoxItem.TouchesCapturedWithin, e.Device);
            }
            else
            {
                devices = new List<InputDevice>(new InputDevice[] { e.Device });
            }

            SurfaceDragCursor cursor = SurfaceDragDrop.BeginDragDrop(List, draggedListBoxItem, cursorVisual, data, devices, DragDropEffects.Copy);

            if (cursor == null)
            {
                return;
            }

            // Reset the input device's state.
            InputDeviceHelper.ClearDeviceState(e.Device);
            ignoredDeviceList.Remove(e.Device);

            draggedListBoxItem.Opacity = 0.5;
            e.Handled = true;
        }
        /// <summary>
        /// Handles the PreviewTouchDown event for the ShoppingList.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnListPreviewTouchDown(object sender, TouchEventArgs e)
        {
            ignoredDeviceList.Remove(e.Device);
            InputDeviceHelper.ClearDeviceState(e.Device);

            InputDeviceHelper.InitializeDeviceState(e.Device);
        }

        /// <summary>
        /// Handles the PreviewTouchUp event for the ShoppingList.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnListPreviewTouchUp(object sender, TouchEventArgs e)
        {
            ignoredDeviceList.Remove(e.Device);
            InputDeviceHelper.ClearDeviceState(e.Device);
        }

        private void OnListDragCompleted(object sender, SurfaceDragCompletedEventArgs e)
        {
            Item draggingData = (Item)e.Cursor.Data;
            if (draggingData != null)
            {
                ResetListBoxItem(draggingData);
            }
        }
        private void OnListDragCanceled(object sender, SurfaceDragDropEventArgs e)
        {
            Item draggingData = (Item)e.Cursor.Data;
            if (draggingData != null)
            {
                ResetListBoxItem(draggingData);
            }
        }

        private void ResetListBoxItem(Item itemData)
        {
            SurfaceListBoxItem sourceListBoxItem = null;
            foreach (object item in List.Items)
            {
                if (((Item)item) == itemData)
                {
 
                    sourceListBoxItem = List.ItemContainerGenerator.ContainerFromItem(item) as SurfaceListBoxItem;
                
                }
            }

            if (sourceListBoxItem != null)
            {
                sourceListBoxItem.Opacity = 1.0;
            }
        }

        /// <summary>
        /// Handles the PreviewTouchMove event for the MeasureList.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnListPreviewTouchMove(object sender, TouchEventArgs e)
        {
            // If this is a touch device whose state has been initialized when its down event happens
            if (InputDeviceHelper.GetDragSource(e.Device) != null)
            {
                StartDragDrop(List, e);
            }
        }

        /// <summary>
        /// Handles the PreviewMouseMove event for the MeasureList.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnListPreviewMouseMove(object sender, MouseEventArgs e)
        {
            // If this is a mouse whose state has been initialized when its down event happens
            if (InputDeviceHelper.GetDragSource(e.Device) != null)
            {
                StartDragDrop(List, e);
            }
        }


        /// <summary>
        /// Handles the PreviewMouseLeftButtonUp event for the ShoppingList.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnListPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ignoredDeviceList.Remove(e.MouseDevice);
            InputDeviceHelper.ClearDeviceState(e.Device);
        }

        /// <summary>
        /// Handles the PreviewMouseLeftButtonDown event for the ShoppingList.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnListPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ignoredDeviceList.Remove(e.Device);
            InputDeviceHelper.ClearDeviceState(e.Device);

            InputDeviceHelper.InitializeDeviceState(e.Device);
        }

        /// <summary>
        /// Merges the remaining input devices on the drag source besides the inpout device that is down.
        /// </summary>
        /// <param name="existingInputDevices"></param>
        /// <param name="extraInputDevice"></param>
        /// <returns></returns>
        private static IEnumerable<InputDevice> MergeInputDevices(IEnumerable<TouchDevice> existingInputDevices, InputDevice extraInputDevice)
        {
            var result = new List<InputDevice> { extraInputDevice };

            foreach (InputDevice inputDevice in existingInputDevices)
            {
                if (inputDevice != extraInputDevice)
                {
                    result.Add(inputDevice);
                }
            }

            return result;
        }

        /// <summary>
        /// Attempts to get an ancestor of the passed-in element with the given type.
        /// </summary>
        /// <typeparam name="T">Type of ancestor to search for.</typeparam>
        /// <param name="descendent">Element whose ancestor to find.</param>
        /// <param name="ancestor">Returned ancestor or null if none found.</param>
        /// <returns>True if found, false otherwise.</returns>
        private static T GetVisualAncestor<T>(DependencyObject descendent) where T : class
        {
            T ancestor = null;
            DependencyObject scan = descendent;
            ancestor = null;

            while (scan != null && ((ancestor = scan as T) == null))
            {
                scan = VisualTreeHelper.GetParent(scan);
            }

            return ancestor;
        }

        // Droptest ob das Gezogene Element auf das Steuerelement gedroppt werden kann. Tritt ein, wenn die DragOperation abgeschlossen ist
        private void Container_PreviewQueryTarget(object sender, QueryTargetEventArgs e)
        {


            if (e.Cursor.Data.ToString() == "MuSeCo_Model.ItemPage")
            {
                //tue nichts, alles Ok
 
            }
            else
            {
                e.UseDefault = false;
                e.ProposedTarget = null;
                e.Handled = true;
            }
        }

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            SurfaceDragDrop.RemovePreviewQueryTargetHandler(this, Container_PreviewQueryTarget);
            Unloaded -= OnUnloaded;
        }


        private void SearchExecute2(string searchString)
        {
            EventAggregator.GetEvent<AddNoteEvent>().Publish("Test Note 1");
        }

        protected static IEventAggregator EventAggregator
        {
            get { return GetService<IEventAggregator>(); }
        }


        protected static T GetService<T>() where T : class
        {
            return ServiceContainer.Instance.GetService<T>();
        }

        #region Kontextmenü

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            //EventAggregator.GetEvent<AddNoteEvent>().Publish(vm.getMeasure1);

          
        }
 

        #endregion
    }
}
