﻿using System.Windows;
using MuSeCo_Model;
using Microsoft.Surface.Presentation;
using System;

namespace MuSeCo_UI.View
{

    /// <summary>
    /// Interaktionslogik für Search Result Control.xaml
    /// </summary>
    /// <seealso cref="MuSeCo_UI.View.BaseView" />
    /// <seealso cref="System.Windows.Markup.IComponentConnector" />
    public partial class SearchResultView : BaseView
    {
        #region ctor
        public SearchResultView()
        {
            InitializeComponent();
            SurfaceDragDrop.AddPreviewQueryTargetHandler(container, Container_PreviewQueryTarget);
            Unloaded += new RoutedEventHandler(OnUnloaded);
        }
        #endregion

        #region PreviewQueryTarget event
        private void Container_PreviewQueryTarget(object sender, QueryTargetEventArgs e)
        {
            if (!(e.Cursor.Data is ItemPage))
                return;
            ItemPage page = (ItemPage)e.Cursor.Data;
            Item parent = page.Parent;
          
            Console.WriteLine(e.Cursor.Data);
            // Nur ursprüngliche Elemente können wieder in diese Quelle gedroppt werden
            /*bool contained = false;
            foreach (object item in container.ItemsSource)
            {
                if (item is ISearchable)
                {
                    if (e.Cursor.Data as ISearchable != null && (e.Cursor.Data as ISearchable).Equals(item))
                    {
                        contained = true;
                        break;
                    }
                }
            }

            if (contained == false)
            {
                e.UseDefault = false;
                e.ProposedTarget = null;
                e.Handled = true;
            }*/
        }
        #endregion

        #region unload event
        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            SurfaceDragDrop.RemovePreviewQueryTargetHandler(this, Container_PreviewQueryTarget);
            Unloaded -= OnUnloaded;
        }
        #endregion
    }
}
