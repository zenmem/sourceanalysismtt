﻿using Microsoft.Practices.Prism.Events;
using Microsoft.Surface.Presentation;
using MuSeCo_ViewModel;
using SoftwareLab.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MuSeCo_UI.View
{
    /// <summary>
    /// Interaktionslogik für SurfaceNote.xaml
    /// </summary>
    public partial class SurfaceNote : BaseView
    {



        public SurfaceNote()
        {
            InitializeComponent();

            SurfaceDragDrop.AddPreviewQueryTargetHandler(GridNote, Container_PreviewQueryTarget);
            Unloaded += new RoutedEventHandler(OnUnloaded);
        }

        private void Container_PreviewQueryTarget(object sender, QueryTargetEventArgs e)
        {
            e.UseDefault = false;
            e.ProposedTarget = null;
            e.Handled = true;
        }

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            SurfaceDragDrop.RemovePreviewQueryTargetHandler(this, Container_PreviewQueryTarget);
            Unloaded -= OnUnloaded;
        }


    }
}
