﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using SoftwareLab.Sys.Windows;
using System.Windows.Media.Animation;
using Microsoft.Surface.Presentation.Controls;

namespace MuSeCo_UI.View
{
    public class BaseView : UserControl
    {
        public BaseView()
        {
            this.Loaded += new System.Windows.RoutedEventHandler(BaseView_Loaded);
            this.Unloaded += new System.Windows.RoutedEventHandler(BaseView_Unloaded);   
        }
        //Tritt ein, wenn das Element angelegt wird, gerendert und für die Interaktion bereit ist.
        void BaseView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            var svi = VisualHelper.GetVisualAncestor<ScatterViewItem>(this);
            if (svi != null)
            {
                svi.Height = this.ActualHeight;
                svi.Width = this.ActualWidth;

                ResourceDictionary LibrayStiles = new ResourceDictionary();
                LibrayStiles.Source = new Uri("/MuSeCo;component/Resources/Theme.xaml", UriKind.RelativeOrAbsolute);

                //wählt den passen Style für eine View beim laden
                if (this is RadialMenu)
                    svi.Style = (Style)LibrayStiles["RadialMenuItemStyle"];
                else if (this is SearchResultView)
                    svi.Style = (Style)LibrayStiles["LibraryContainerInScatterViewItemStyle"];
                else if (this is SurfaceNote)
                    svi.Style = (Style)LibrayStiles["SurfaceNoteAreaStyle"];
                else if (this is TaktCompare)
                    svi.Style = (Style)LibrayStiles["TaktCompareStyle"];

            }
        }

        void BaseView_Unloaded(object sender, System.Windows.RoutedEventArgs e)
        {
            this.Loaded -= new System.Windows.RoutedEventHandler(BaseView_Loaded);
            this.Unloaded -= new System.Windows.RoutedEventHandler(BaseView_Unloaded);
        }
    }
}
