﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuSeCo_ViewModel.Gesture
{
    /*
     * Enum mit den möglichen Gesten innerhalb des Widgets
     */
    public enum MyGestureSet
    {
        NONE = 0,
        CIRCLE = 1,
        FLICK_LEFT = 2
    }
}
