﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using MuSeCo_Model;
using System.Collections.Generic;
using Microsoft.Practices.Prism.Commands;
using System.Windows.Forms;
using System.IO;
using MuSeCo_ViewModel.Controls;

namespace MuSeCo_ViewModel
{
    public class SearchResultViewModel : ViewModelBase, ISearchable
    {
        #region fields
        private DelegateCommand<ISearchable> _removeCommand;
        private ObservableCollection<Item> _Items;
        private ObservableCollection<ItemPage> _ItemPages;
     
       // private ImageProcessor p;
        private string _label;
        private System.Windows.Media.SolidColorBrush _color;
        //private List<System.String> mItem;
        #endregion

        #region ctor
        public SearchResultViewModel()
        {
          //  p = new ImageProcessor();

         



            _Items = new ObservableCollection<Item>();
            _ItemPages = new ObservableCollection<ItemPage>();

            FolderBrowserDialog fbd = new FolderBrowserDialog();

            #region Logik für das Hinzufügen einer Itempage zu der Quellenverwaltung

            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string[] files = Directory.GetFiles(fbd.SelectedPath);
               // System.Tuple<List<System.String>, byte,byte,byte> test = p.getMarkedItem(files);
               // mItem = test.Item1;

                Label = Path.GetFileName(Path.GetDirectoryName(files[0]));

                try
                {
                    //ObservableCollection<Image> items = new ObservableCollection<Image>();
                    int index = 0;
                    Item item = new Item();
                    foreach (string file in files)
                    {
                        
                        ItemPage temp = new ItemPage(item);
                        temp.ThumbnailUrl = file;
                        temp.Url = file;
                        temp.Label = Path.GetFileName(Path.GetDirectoryName(file).Remove(Path.GetDirectoryName(file).Length-7));//????
                                                                                                                                //   
                        //BColor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, test.Item2, test.Item3, test.Item4)); 

                        //MessageBox.Show(""+ Path.GetFileName(Path.GetDirectoryName(file)));
                        //temp.Id = index;
                        //temp.Label = "file" + index;
                        temp.Id = index;
                        index++;
                     //   temp.Measures = _measureGroup2;
                        item.Pages.Add(temp);
                        _ItemPages.Add(temp);
                        //Image test2 = LoadImageFromPath(file);
                        //items.Add(test2);
                        //lbartest.Items.Add(test2);

                    }
                }
                catch (System.IO.DirectoryNotFoundException)
                {

                    MessageBox.Show("HIER IST WAS SCHIEF GELAUFEN");
                }

            }
            #endregion

            //Delegiert das Schließen KOmmando zum SUrfaceShellViemodel das dann dieses Model aus der ItemSource der Haupt-View löscht
            _removeCommand = new DelegateCommand<ISearchable>(RemoveExecute);
          
        }
        #endregion

        #region public properties
        public ObservableCollection<ItemPage> ItemPages
        {
            get { return _ItemPages; }
            set
            {
                _ItemPages = value;
                RaisePropertyChanged("ItemPages");
            }
        }

        public ObservableCollection<Item> Items
        {
            get { return _Items; }
            set
            {
                _Items = value;
                RaisePropertyChanged("Items");
            }
        }



        public string Label
        {
            get
            {
                return _label;
            }
            set
            {
                _label = value;
                RaisePropertyChanged("Label");


            }
        }

   
        #endregion

        #region commands
        public ICommand RemoveCommand
        {
            get
            {
                return _removeCommand;
            }
        }
        #endregion

        #region private methods
        private void RemoveExecute(ISearchable searchable)
        {
            EventAggregator.GetEvent<RemoveEvent>().Publish(searchable);
        }
        #endregion
    }
}