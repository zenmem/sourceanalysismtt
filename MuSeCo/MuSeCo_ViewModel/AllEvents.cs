﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Events;
using MuSeCo_Model;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation;

namespace MuSeCo_ViewModel
{
    public class AddSourceEvent : CompositePresentationEvent<string> { }
    public class RemoveEvent : CompositePresentationEvent<ISearchable> { }
    public class AddEvent : CompositePresentationEvent<ISearchable> { }
    public class AddNoteEvent : CompositePresentationEvent<string> { }
    public class AddPictureCompareEvent : CompositePresentationEvent<string> { }
    public class RotationEvent : CompositePresentationEvent<string> { }

    public class AddScatterItemEvent : CompositePresentationEvent<ScatterViewItem> { }


}
