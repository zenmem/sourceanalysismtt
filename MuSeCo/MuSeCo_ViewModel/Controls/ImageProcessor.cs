﻿using MuSeCo_Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;

namespace MuSeCo_ViewModel.Controls
{
    /// <summary>
    /// Liefert eine Klasse zum verarbeiten von Itempages
    /// </summary>
    public class ImageProcessor
    {

        #region Deklarationen

        private Tuple<List<string>, byte, byte, byte> _markedItem;
        private List<string> _markedItempageList;
        private Random _rnd;
        private System.Drawing.Rectangle rect3 = new System.Drawing.Rectangle();
        private string newImagepath;
        #endregion

        #region Konstruktor

        /// <summary>
        /// Initialisiert eine neue Instanz der <see cref="ImageProcessor"/> Klasse.
        /// </summary>
        public ImageProcessor()
        {
            _markedItempageList = new List<string>();
            _rnd = new Random();
        }

        #endregion

        #region Methoden

      

        /// <summary>
        /// Streamt die zugeschnittenen Takte der Itemapges auf die Festplatte.
        /// </summary>
        /// <param name="path">Speicherort der zugeschnittenen Takte der Itemapge</param>
        /// <param name="cb">Zugeschnittener Takt der Itemapge</param>
        public void streamCroppedBitmap(string path, CroppedBitmap cb)
        {
            using (FileStream fs = File.Create(path))
            {
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(cb));
                encoder.Save(fs);
                fs.Close();
            }
        }



        /// <summary>
        /// Verbindet zwei Takte einer Itempage.
        /// </summary>
        /// <param name="firstimagePath">Erster Takt</param>
        /// <param name="secondImagePath">Zweiter Takt</param>
        /// <returns>
        /// Pfad der verbundenen Takte 
        /// </returns>
        public string cainImages(string firstimagePath, string secondImagePath)
        {
            //Erzeugt den Speicherpfad
            string parentdirectory = Path.GetDirectoryName(firstimagePath);
            string savePath = parentdirectory + @"\"+ Path.GetFileName(firstimagePath)+"_"+Path.GetFileName(secondImagePath+".bmp");

            //Erteugt zwei Bilder aus den Pfaden der zugeschnittenen Takte einer Itempage
            System.Drawing.Image img1 = System.Drawing.Image.FromFile(firstimagePath);
            System.Drawing.Image img2 = System.Drawing.Image.FromFile(secondImagePath);

            //Setzt die neue Höhe und Breite der zusammengefügten Takte fest
            int width = img1.Width + img2.Width;
            int height = Math.Max(img1.Height, img2.Height);

            //Erzeugt das neue Bild mit der neuen Höhe und Breite
            System.Drawing.Bitmap img3 = new System.Drawing.Bitmap(width, height);

            //Erzeugt ein Graphics Objekt wo die beiden Takte die verbunden werden sollen, drauf gezeichnet werden
            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(img3);
            g.Clear(System.Drawing.Color.White);
            g.DrawImage(img1, new System.Drawing.Point(0, 0));
            g.DrawImage(img2, new System.Drawing.Point(img1.Width, 0));

            g.Dispose();
            img1.Dispose();
            img2.Dispose();

            ////kann gelöscht werden
            //var bitmapImage = new BitmapImage();
            //using (var memory = new MemoryStream())
            //{
            //    img3.Save(memory, System.Drawing.Imaging.ImageFormat.Png);
            //    memory.Position = 0;

            //    bitmapImage.BeginInit();
            //    bitmapImage.StreamSource = memory;
            //    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
            //    bitmapImage.EndInit();


            //}

            //Speichert das neu Erzeugte Bild auf der Festplatte
            img3.Save(savePath, System.Drawing.Imaging.ImageFormat.Jpeg);
            img3.Dispose();

            //Gibt den Speicherort des neuen Bildes der verbundenen Takte zurück
            return savePath;
        }


        /// <summary>
        /// Liefert die random farblich markierten Itempages eines Items
        /// </summary>
        /// <param name="files">Liste der Speicherorte der ursprünglichen Pfade von Itempages</param>
        /// <returns>
        /// Gibt ein Tuple bestehend aus einer Liste der neuen Speicherorte für die farblich markierten Itempages und die dazugehörogen RGB Werte zurück 
        /// </returns>
        public Tuple<List<String>, byte, byte, byte> getMarkedItem(string[] files)
        {

            //Random RGB Werte für die Hintergrund-, und Rahmenfarbe
            byte r = (byte)_rnd.Next(0, 255);
            byte g = (byte)_rnd.Next(0, 255);
            byte b = (byte)_rnd.Next(0, 255);

            //Random Farbe
            System.Drawing.Color randomColor = System.Drawing.Color.FromArgb(r, g, b);

            //Holt Überverzeichnis des Verzeichnisses wo die Itempages gespeichert sind
            string parentdirectory = Path.GetDirectoryName(Path.GetDirectoryName((files[0])));

            //Erstellt neuen Ordner für die markierten Itempages 
            string newMarkedDirectory = System.IO.Path.Combine(parentdirectory, Path.GetDirectoryName((files[0])) + "_marked");
            System.IO.Directory.CreateDirectory(newMarkedDirectory);

            //Für jede Itempage eines Items wird eine Markierung gezeichnet
            foreach (string source in files)
            {
                // Erstellt Bitmap aus der Itempage Source
                System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(source);

                // Erstellt ein Graphics Objekt aus dem Bitmap um eine Oberfläche zu bekommen auf der man zeichnen kann
                System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(bmp);

                //Erstellt einen Stift mit der Random Farbe und Dicke 
                System.Drawing.Pen drawingPen = new System.Drawing.Pen(randomColor, 25);

                //Zeichnet mit dem Stift die Markierungsliene am unteren Rand der Itempage
                graphics.DrawLine(drawingPen, 0, bmp.Height, bmp.Width, bmp.Height);

                //Speichert die markierte Itempage in dem neuen Ordner für markierte Itempages
                string filename = Path.GetFileName(source).Remove(Path.GetFileName(source).Length - 4);
                String pathMarkedItempage = newMarkedDirectory + @"\" + filename + ".jpg";
                bmp.Save(pathMarkedItempage, System.Drawing.Imaging.ImageFormat.Jpeg);

                //Fügt die Source der markierten Itempage in eine Liste ein und übergibt diese zusammen mit dem Random RGB Wert dem Aufrufer
                _markedItempageList.Add(pathMarkedItempage);
            }
            _markedItem = new Tuple<List<String>, byte, byte, byte>(_markedItempageList, r, g, b);

            return _markedItem;
        }


        /// <summary>
        /// Liefert bei Eingabe von 2 Rechtecken die Überscheidung in Form eines neuen Rechtecks
        /// </summary>
        /// <param name="rect1">Erstes Rechteck</param>
        /// <param name="rect2">Zweites Rechteck</param>
        /// <returns></returns>
        private System.Drawing.Rectangle getIntersection(System.Drawing.Rectangle rect1, System.Drawing.Rectangle rect2)
        {
            System.Drawing.Rectangle rectangle3 = new System.Drawing.Rectangle();

            if (rect1.IntersectsWith(rect2))
            {
                rectangle3 = System.Drawing.Rectangle.Intersect(rect1, rect2);

                return rectangle3;
            }
            else
            {

                return new System.Drawing.Rectangle(0, 0, 0, 0);
            }

        }


        /// <summary>
        /// Macht ein Bild durch den Alpha wert einer RGB Farbe transparent
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="Alpha">The alpha.</param>
        /// <returns></returns>
        private System.Drawing.Image SetImageOpacity(System.Drawing.Image image, Byte Alpha)
        {
            try
            {
                System.Drawing.Bitmap Original = new System.Drawing.Bitmap(image);
                System.Drawing.Bitmap TImage = new System.Drawing.Bitmap(image.Width, image.Height);


 

                System.Drawing.Color c = System.Drawing.Color.Black;
                System.Drawing.Color v = System.Drawing.Color.Black;

                for (int i = 0; i < image.Width; i++)
                {
                    for (int y = 0; y < image.Height; y++)
                    {
                        c = Original.GetPixel(i, y);
                        v = System.Drawing.Color.FromArgb(Alpha, c.R, c.G, c.B);
                        TImage.SetPixel(i, y, v);
                    }
                }

                return TImage;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
