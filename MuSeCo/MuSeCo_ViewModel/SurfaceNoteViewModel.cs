﻿using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Win32;
using MuSeCo_Model;
using MuSeCo_ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;

namespace MuSeCo_ViewModel
{
    public class SurfaceNoteViewModel : ViewModelBase, ISearchable
    {


        private double fsize;

        private DelegateCommand<ISearchable> _removeCommand;
        private  string _ContentTXT = "";
        private string _ContentLbl;
        private Stack<string> undoList = new Stack<string>();
        private Stack<string> redoList = new Stack<string>();
        private int _FontSize = 20;

        private bool look = false;

        public SurfaceNoteViewModel(string temp)

        {
            ContentLbl = temp;
            _removeCommand = new DelegateCommand<ISearchable>(RemoveExecute);

        }


        public string ContentTXT
        {
            get
            {
                return _ContentTXT;
            }
            set
            {
                _ContentTXT = value;
                if (!look)
                {
                    undoList.Push(_ContentTXT);
                }
                look = false;

                RaisePropertyChanged("ContentTXT");

            }
        }



        public string ContentLbl
        {
            get
            {
                return _ContentLbl;
            }
            set
            {
                _ContentLbl = value;
                RaisePropertyChanged("ContentLbl");

            }
        }


        public int FSize
        {
            get
            {
                return _FontSize;
            }
            set
            {
                _FontSize = value;
                RaisePropertyChanged("FSize");

            }
        }



      
        private void RemoveExecute(ISearchable searchable)
        {
            EventAggregator.GetEvent<RemoveEvent>().Publish(searchable);
        }

        #region Kommanods des Editors

        public ICommand RemoveCommand
        {
            get
            {
                return _removeCommand;
            }
        }



        public ICommand sbtnbigger
        {
            get
            {
                return new RelayCommand(() =>
                {

                    FSize = FSize + 5;


                });
            }
        }

        

        public ICommand sbtnsmaller
        {
            get
            {
                return new RelayCommand(() =>
                {

                    FSize = FSize - 5;


                });
            }
        }

        [DllImport("user32.dll", SetLastError = true)]
        static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

        public const int KEYEVENTF_EXTENDEDKEY = 0x0001; //Key down flag
        public const int KEYEVENTF_KEYUP = 0x0002; //Key up flag
        public const int VK_LCONTROL = 0xA2; //Left Control key code
        public const int A = 0x41; //A key code
        public const int C = 0x43; //C key code
        public const int V = 0x56; // V Key Code
        public const int X = 0x58; // V Key Code



        public ICommand sbtncopy
        {
            get
            {
                return new RelayCommand(() =>
                {

                    // Hold Control down and press C
                    keybd_event(VK_LCONTROL, 0, KEYEVENTF_EXTENDEDKEY, 0);
                    keybd_event(C, 0, KEYEVENTF_EXTENDEDKEY, 0);
                    keybd_event(C, 0, KEYEVENTF_KEYUP, 0);
                    keybd_event(VK_LCONTROL, 0, KEYEVENTF_KEYUP, 0);
                });
            }
        }

        public ICommand sbtncut
        {
            get
            {
                return new RelayCommand(() =>
                {

                    //if (redoList.Count>0)
                    //{
                    //    string temp = redoList.Pop();
                    //    undoList.Push(temp);
                    //    look = true;
                    //    ContentTXT = temp;
                    //}

                    // Hold Control down and press X
                    keybd_event(VK_LCONTROL, 0, KEYEVENTF_EXTENDEDKEY, 0);
                    keybd_event(X, 0, KEYEVENTF_EXTENDEDKEY, 0);
                    keybd_event(X, 0, KEYEVENTF_KEYUP, 0);
                    keybd_event(VK_LCONTROL, 0, KEYEVENTF_KEYUP, 0);


                });
            }
        }

        public ICommand sbtnpaste
        {
            get
            {
                return new RelayCommand(() =>
                {


                    // Hold Control down and press C
                    keybd_event(VK_LCONTROL, 0, KEYEVENTF_EXTENDEDKEY, 0);
                    keybd_event(V, 0, KEYEVENTF_EXTENDEDKEY, 0);
                    keybd_event(V, 0, KEYEVENTF_KEYUP, 0);
                    keybd_event(VK_LCONTROL, 0, KEYEVENTF_KEYUP, 0);


                });
            }
        }


        public ICommand sbtnundo
        {
            get
            {
                return new RelayCommand(() =>
                {


                    //MessageBox.Show(undoList.Count()+"");
                    if (undoList.Count>0)
                    {
                        string temp = undoList.Pop();
                        redoList.Push(temp);
                        look = true;
                        ContentTXT = temp;
                    }



                });
            }
        }


        public ICommand sbtnSave
        {
            get
            {
                return new RelayCommand(() =>
                {

                    SaveFileDialog saveFile = new SaveFileDialog();
                    saveFile.Filter = "XAML Files (*.xaml)|*.xaml|RichText Files (*.rtf)|*.rtf|All Files (*.*)|*.*";
                    saveFile.FileName = ContentLbl;

                    if (saveFile.ShowDialog() == true)
                    {

                        using (Stream s = File.Open(saveFile.FileName, FileMode.CreateNew))
                        using (StreamWriter sw = new StreamWriter(s))
                        {
                            sw.Write(ContentTXT);
                        }
                    }


                });
            }
        }

        public ICommand sbtnLoad
        {
            get
            {
                return new RelayCommand(() =>
                {

                    Stream s;
                    OpenFileDialog openFile = new OpenFileDialog();
                    openFile.Filter = "XAML Files (*.xaml)|*.xaml|RichText Files (*.rtf)|*.rtf|All Files (*.*)|*.*";

                    if (openFile.ShowDialog() == true)
                    {

                        if ((s = openFile.OpenFile()) != null)
                        {
                            string filename = openFile.FileName;
                            string filetxt = File.ReadAllText(filename);
                            ContentTXT = filetxt;
                        }

                    }


                });
            }
        }

        public ICommand sbtnClose
        {
            get
            {
                
            
                    return _removeCommand;
                
            }
        }

        public ICommand sbtnPrint
        {
            get
            {
                return new RelayCommand(() =>
                {

                    PrintDialog printDialog = new PrintDialog();
                    printDialog.PageRangeSelection = PageRangeSelection.AllPages;
                    printDialog.UserPageRangeEnabled = true;

                    if (printDialog.ShowDialog() == true)
                    {
                        //printDialog.PrintVisual(stxtBox as Visual,"print as Visual" );
                        //printDialog.PrintDocument((IDocumentPaginatorSource)stxtBox.Doc);

                        // create a document
                        FixedDocument document = new FixedDocument();
                        document.DocumentPaginator.PageSize = new Size(printDialog.PrintableAreaWidth, printDialog.PrintableAreaHeight);

                        // create a page
                        FixedPage page1 = new FixedPage();
                        page1.Width = document.DocumentPaginator.PageSize.Width;
                        page1.Height = document.DocumentPaginator.PageSize.Height;

                        // add some text to the page
                        TextBlock page1Text = new TextBlock();
                        page1Text.Text = ContentTXT;
                        page1Text.FontSize = 40; // 30pt text
                        page1Text.Margin = new Thickness(96); // 1 inch margin
                        page1.Children.Add(page1Text);

                        // add the page to the document
                        PageContent page1Content = new PageContent();
                        ((IAddChild)page1Content).AddChild(page1);
                        document.Pages.Add(page1Content);


                        printDialog.PrintDocument(document.DocumentPaginator, "My first document");
                    }


                });
            }
        }

        #endregion

    }
}
