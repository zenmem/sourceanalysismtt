﻿using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Prism.Commands;
using MuSeCo_Model;
using MuSeCo_ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;


namespace MuSeCo_ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="MuSeCo_ViewModel.ViewModelBase" />
    /// <seealso cref="MuSeCo_Model.ISearchable" />
    public class RadialMenuViewModel : ViewModelBase, ISearchable
    {

        private DelegateCommand<string> _searchCommand;
        private DelegateCommand<string> _addNoteCommand;
        private DelegateCommand<string> _rotateCommand;
        private DelegateCommand<string> _addPictureCompareCommand;
        private int _opacityGrade = 1;
        private bool _isOpen = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="RadialMenuViewModel"/> class.
        /// </summary>
        public RadialMenuViewModel()
        {
            //delegiert bei Aufruf des entsprchenden KOmmanods des Zentralsmünus die Kommanods zur SurfaceShellViemodel
            _searchCommand = new DelegateCommand<string>(SearchExecute);
            _addNoteCommand = new DelegateCommand<string>(SearchExecute2);
            _rotateCommand= new DelegateCommand<string>(SearchExecute4);
            _addPictureCompareCommand = new DelegateCommand<string>(SearchExecute3);

        }



        #region Verhalten des Zentralmenüs bei touch auf Menü-Button

        /// <summary>
        /// Gets or sets the state of the opacity.
        /// </summary>
        /// <value>
        /// The state of the opacity.
        /// </value>
        public int OpacityState
        {
            get
            {
                return _opacityGrade;
            }
            set
            {

                _opacityGrade = value;
                RaisePropertyChanged("OpacityState");
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether this instance is open.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is open; otherwise, <c>false</c>.
        /// </value>
        public bool IsOpen
        {
            get
            {
                return _isOpen;
            }
            set
            {

                _isOpen = value;
                RaisePropertyChanged("IsOpen");
            }
        }
        #endregion

        #region

        public ICommand OpenRadialMenu
        {
            get
            {
                return new RelayCommand(() =>
                {

                    IsOpen = true;
                    OpacityState = 0;
                    //iscliped = new EllipseGeometry(new Point(150, 150), 150, 150);

                });
            }
        }

        public ICommand CloseRadialMenu
        {
            get
            {
                return new RelayCommand(() =>
                {
                    IsOpen = false;
                    OpacityState = 1;

                });
            }
        }
        #endregion

        #region Kommandos des Zentralmünus

        public ICommand AddSource
        {
            get
            {
                return _searchCommand;
            }
        }

        public ICommand AddNoteCommand
        {
            get
            {
                return _addNoteCommand;
            }
        }

        public ICommand RotateCommand
        {
            get
            {
                return _rotateCommand;
            }
        }

        #endregion


        private void SearchExecute(string searchString)
        {
            EventAggregator.GetEvent<AddSourceEvent>().Publish(searchString);
        }

        private void SearchExecute2(string searchString)
        {
            EventAggregator.GetEvent<AddNoteEvent>().Publish("Test Note");
        }

        private void SearchExecute3(string searchString)
        {
            EventAggregator.GetEvent<AddPictureCompareEvent>().Publish(searchString);
        }

        private void SearchExecute4(string searchString)
        {
            EventAggregator.GetEvent<RotationEvent>().Publish(searchString);
        }


    }
}
