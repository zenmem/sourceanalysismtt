﻿using System;
using System.ComponentModel;
using System.Windows;
using Microsoft.Practices.Prism.Events;
using SoftwareLab.Sys;
using SoftwareLab.Sys.ComponentModel;

namespace MuSeCo_ViewModel
{
    /// <summary>
    /// Represents a ViewModelBase.  This is the base class for all ViewModels, providing wrappers for services and INotifyPropertyChanged implementation.
    /// </summary>
    public abstract class ViewModelBase : ObservableObject
    {
        #region ctor
        public ViewModelBase()
        {
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                ServiceLoader.LoadDesignTimeServices();
            }
        }
        #endregion

        /// <summary>
        /// Gets the IEventAggregator registered with the ServiceContainer.
        /// </summary>
        protected static IEventAggregator EventAggregator
        {
            get { return GetService<IEventAggregator>(); }
        }

        /// <summary>
        /// Gets the requested service.
        /// </summary>
        /// <typeparam name="T"><see cref="Type"/> of Service to return</typeparam>
        protected static T GetService<T>() where T : class
        {
            return ServiceContainer.Instance.GetService<T>();
        }       
    }
}
