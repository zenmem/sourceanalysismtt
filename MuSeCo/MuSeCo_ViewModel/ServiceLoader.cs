﻿using Microsoft.Practices.Prism.Events;
using SoftwareLab.Sys;

namespace MuSeCo_ViewModel
{
    public static class ServiceLoader
    {
        public static void LoadDesignTimeServices()
        {
            ServiceContainer.Instance.AddService<IEventAggregator>(new EventAggregator());
        }

        public static void LoadRunTimeServices()
        {
            ServiceContainer.Instance.AddService<IEventAggregator>(new EventAggregator());
        }
    }
}
