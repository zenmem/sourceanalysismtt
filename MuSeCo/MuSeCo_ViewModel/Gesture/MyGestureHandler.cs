﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Surface.Presentation.Controls;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Collections.ObjectModel;
using MuSeCo_Model;
using System.Windows.Threading;

namespace MuSeCo_ViewModel.Gesture
{
    class MyGestureHandler
    {
        private ScatterView _scatterView;            //Grundfläche aus dem MainSurfaceWindow        
        //private LibraryContainer libraryContainer= new LibraryContainer();  //Container auf dem ScatterView, der bei Widget-Ausführung eingeblendet wird        
        private ScatterViewItem targetArea;         //Kreis, welcher das Zielgebiet der Items anzeigt                         
        private Storyboard stb;                     //Rahmen für die Animationen der Items
        ObservableCollection<ScatterViewItem> _searchableCollection;
        /*
         * Konstruktor.
         * Zeiger auf den Haupt-ScatterView: Um die ScatterViewItems später passend zur Geste zu verarbeiten
         * Zeiger auf den Library-Container: Einblendung des Containers
         * Zeiger auf das Zielgebiet - Item: Ein-/Ausblenden als Hilfestellung
         */
        public MyGestureHandler(ScatterView scatterView, ObservableCollection<ScatterViewItem> searchableCollection,  ScatterViewItem targetArea)
        {
            _searchableCollection = searchableCollection;
            _scatterView = scatterView;
            //this.libraryContainer = libraryContainer;
            this.targetArea = targetArea;
        }






        /*
         * Diese Methode wird bei Erkennung einer Kreis-Geste ausgeführt und zieht alle Bild-Items schrittweise zur letzten Touch-Position
         */
        private void ItemsToPoint(Point p)
        {
            Console.WriteLine("MyGestureHandler_ItemsToPoint() -> Point:{0},{1}", p.X, p.Y);
            stb = new Storyboard();
            List<Point> endPoints = new List<Point>(); //Benötigt, um sich Endpunkte der Items zu merken (müssen nach Animation erneut gesetzt werden)
            Point endPoint = p;

            foreach (ScatterViewItem item in _searchableCollection)
            {
               
                    PointAnimation moveItem = new PointAnimation();
                    Vector distance = p - item.ActualCenter;

                    double newDistanceFactor = 1.0; //Faktor, wo Widget nach Pull-Operation landen soll (1.0=Entfernung bleibt gleich)
                    if (distance.Length < 125.0)
                    {

                    MessageBox.Show("endpunkt");
                        //Direkt ranziehen, wenn bereits im Kreis angekommen
                        endPoint = p;
                    }
                    else //Wenn Item weiter entfernt, bestimme passenden Faktor für "flüssige" Bewegung
                    {
                        if (distance.Length < 250)
                            newDistanceFactor = 0.4;
                        else if (distance.Length < 400)
                            newDistanceFactor = 0.5;
                        else if (distance.Length < 600)
                            newDistanceFactor = 0.6;
                        else
                            newDistanceFactor = 0.7;

                        double a; double a_new;
                        double b; double b_new;
                        double c; double c_new;
                        double alpha;
                        double beta;
                        a = p.Y - item.ActualCenter.Y;
                        b = p.X - item.ActualCenter.X;
                        c = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
                        alpha = Math.Asin(a / c);
                        beta = Math.Asin(b / c);
                        c_new = c * newDistanceFactor;
                        a_new = c_new * Math.Sin(alpha);
                        b_new = c_new * Math.Sin(beta);

                        endPoint = new Point(p.X - b_new, p.Y - a_new);
                    }

                    moveItem.From = item.ActualCenter;
                    moveItem.To = endPoint;
                    endPoints.Add(endPoint);
                    moveItem.Duration = new Duration(TimeSpan.FromSeconds(1.0));
                    moveItem.FillBehavior = FillBehavior.Stop;
                     Storyboard.SetTarget(moveItem,item);
                    Storyboard.SetTargetProperty(moveItem, new PropertyPath(ScatterViewItem.CenterProperty));
                     stb.Children.Add(moveItem);

            }
            foreach (ScatterViewItem item in _searchableCollection)
            {
                stb.Begin(item);

            }
            foreach (ScatterViewItem item in _searchableCollection)
            {
               
                    item.Center = endPoints.ElementAt(0);
                    endPoints.RemoveAt(0);
                
            }
            endPoints = null;

            

        }

        /*
         * Methode blendet den LibraryContainer passend zur Posture-Position ein und bestückt ihn mit den passenden Items
         */
        //private void CreateLibraryContainer(Point itemsCenter, Point desiredPosition)
        //{
        //    Console.WriteLine("MyGestureHandler_CreateLibraryContainer() -> Item-Pos:{0},{1} Container-Pos:{2},{3}", itemsCenter.X, itemsCenter.Y, desiredPosition.X, desiredPosition.Y);

        //    List<String> itemsToLibrary = new List<String>();                  //Strings (Bilder), die vom SV in die Library kopiert werden
        //    List<ScatterViewItem> itemsToDelete = new List<ScatterViewItem>(); //ScatterViewItems, die vom SV in die Library kopiert werden
        //    ScatterViewItem libraryContainerContainer = this.libraryContainer.Parent as ScatterViewItem; //ScatterViewItem, welches die Library enthält

        //    //Bestimme welche Items in den Container aufgenommen werden müssen (Distanz < 125px)
        //    foreach (ScatterViewItem item in this.scatterView.Items)
        //    {
        //        double distanceItemVsTouch = Math.Sqrt(Math.Pow(itemsCenter.X - item.ActualCenter.X, 2) + Math.Pow(itemsCenter.Y - item.ActualCenter.Y, 2));
        //        if (item.Content is Image && (distanceItemVsTouch < 125.0))
        //        {
        //            itemsToLibrary.Add(item.Tag as String);
        //            itemsToDelete.Add(item);
        //        }
        //    }

        //    //Entferne überflüssige ScatterViewItems
        //    foreach (ScatterViewItem svi in itemsToDelete)
        //    {
        //        this.scatterView.Items.Remove(svi);
        //    }
        //    itemsToDelete = null;
        //    this.targetArea.Visibility = Visibility.Hidden;

        //    //Neuen LibraryContainer erstellen und ScatterViewItems einfügen
        //    if (this.libraryContainer.ItemsSource != null) //Damit man später weitere items durch die Geste hinzufügen kann
        //    {
        //        foreach (object obj in this.libraryContainer.ItemsSource)
        //        {
        //            itemsToLibrary.Add(obj as string);
        //        }
        //    }
        //    this.libraryContainer.ItemsSource = itemsToLibrary;
        //    this.libraryContainer.ViewingMode = LibraryContainerViewingMode.Stack;

        //    //Nach der Aktion den Container "in der Handfläche" einblenden (desiredPosition)                                              
        //    libraryContainerContainer.Center = desiredPosition;
        //    libraryContainerContainer.Orientation = 0.0;
        //    libraryContainerContainer.Visibility = Visibility.Visible;
        //}

        /*
         * Zentrale Methode. Wird aus dem MainSurfaceWindow aufgerufen, sobald eine Geste durch den MyGestureRecognizer entdeckt wurde
         */
        public void HandleGesture(MyGestureSet gestureResult, bool isPosture, Point center)
        {
            Console.WriteLine("MyGestureHandler_HandleGesture({0}) -> POS:{1},{2} isPosture={3}", gestureResult, center.X, center.Y, isPosture);

            //Zielgebiet nicht sichtbar ausrichten                        
            this.targetArea.Center = center;
            this.targetArea.SetRelativeZIndex(RelativeScatterViewZIndex.Topmost);
            this.targetArea.Visibility = Visibility.Hidden;

            if (isPosture)
            {
                switch (gestureResult)
                {
                    case MyGestureSet.CIRCLE:
                        //Elemente heranholen
                        this.ItemsToPoint(center);

                        //Zielgebiet sichtbar machen                                                
                        this.targetArea.Visibility = Visibility.Visible;
                        break;
                    //case MyGestureSet.FLICK_LEFT:
                    //    //Container erstellen
                    //    //this.CreateLibraryContainer(this.targetArea.ActualCenter, center);
                    //    break;
                }
            }
        }
    }
}
