﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Diagnostics;

namespace MuSeCo_ViewModel.Gesture
{
    /*
    * Die Klasse soll erkennen, ob eine Halte-Geste mit mindestens 4 Fingern ausgeführt wurde.
    * Dann gibt die Funktion isPosture den Wert true zurück.
    */
    class MyPostureRecognizer
    {
        private bool isPosture;                 //Wurde Posture erkannt?
        private List<TimedPoint> holdPositions; //Liste mit allen Hold-Events (ID,Position,Zeit)
        private TimedPoint[] posturePositions;  //Liste mit allen Hold-Fingern, sobald Posture ausgeführt wurde
        private const byte DesiredFingers = 5;  //Anzahl Finger, die für Posture nötig sind

        public MyPostureRecognizer()
        {
            this.isPosture = false;
            this.holdPositions = new List<TimedPoint>();
            this.posturePositions = null;
        }

        /*
         * Wird bei Hold-Event ausgeführt und fügt aktuelle Hold-Position hinzu
         */
        public void AddHoldPosition(int fingerID, Point p)
        {
            Console.WriteLine("MyPostureRecognizer_AddHoldPosition() -> Pos:{0},{1} fingerID={2}", p.X, p.Y, fingerID);
            holdPositions.Add(new TimedPoint(fingerID, p, DateTime.UtcNow.TimeOfDay.TotalMilliseconds));
            this.isPosture = CheckPosture();
        }

        /*
         * Wird bei TouchUp-Event ausgeführt und entfernt Hold-Position anhand der Touch-ID
         */
        public void RemoveHoldPosition(int fingerID)
        {
            Console.WriteLine("MyPostureRecognizer_RemoveHoldPosition() -> fingerID={0}", fingerID);

            //Falls die ID zu einem Posture-Finger gehört, wird nach dem Remove keine Posture
            //mehr ausgeführt. Setze dann den Recognizer zurück.
            if (this.posturePositions != null)
            {
                foreach (TimedPoint tp in this.posturePositions)
                {
                    if (tp.GetID() == fingerID)
                        this.Reset();
                }
            }
            else //Es wird aktuell keine Posture ausgeführt
            {
                TimedPoint toRemove = null;
                foreach (TimedPoint tp in this.holdPositions)
                {
                    if (fingerID == tp.GetID())
                    {
                        toRemove = tp;
                    }
                }
                if (toRemove != null)
                {
                    this.holdPositions.Remove(toRemove);
                    toRemove = null;
                }
            }
        }

        /*
         * Überprüft, ob Posture ausgeführt wird
         */
        private bool CheckPosture()
        {
            Console.WriteLine("MyPostureRecognizer_CheckPosture()");
            bool result = false;
            if (this.holdPositions.Count >= DesiredFingers)
            {
                List<TimedPoint> potentialPoints = this.holdPositions.GetRange(this.holdPositions.Count - DesiredFingers, DesiredFingers);
                double timeSpan = potentialPoints.ElementAt(potentialPoints.Count - 1).GetTime() - potentialPoints.ElementAt(0).GetTime();
                if (timeSpan < 5000.0) //Touchfolge geschah innerhalb von 5 Sekunden
                {
                    double maxDistance = 0;
                    for (int i = 0; i < potentialPoints.Count - 1; i++) //Vergleiche alle Hold-Positionen untereinander
                    {
                        for (int j = i + 1; j < potentialPoints.Count; j++)
                        {
                            double currentDistance = Math.Sqrt(Math.Pow(potentialPoints.ElementAt(i).GetPoint().X - potentialPoints.ElementAt(j).GetPoint().X, 2) + Math.Pow(potentialPoints.ElementAt(i).GetPoint().Y - potentialPoints.ElementAt(j).GetPoint().Y, 2));
                            if (currentDistance > maxDistance)
                                maxDistance = currentDistance;
                        }
                        if (maxDistance < 500.0)
                            result = true;
                    }

                }
                //Falls Posture ausgeführt, speichere diese 5 Finger
                if (result)
                {
                    this.posturePositions = new TimedPoint[DesiredFingers];
                    potentialPoints.CopyTo(this.posturePositions);
                }
            }
            return result;
        }

        /*
         * Setzt den aktuellen Posture-Zustand zurück
         */
        public void Reset()
        {
            Console.WriteLine("MyPostureRecognizer_Reset()");
            this.isPosture = false;
            this.holdPositions = new List<TimedPoint>();
            this.posturePositions = null;
        }

        /*
         * Gibt den aktuellen Posture-Zustand zurück
         */
        public bool IsPosturePerformed()
        {
            return this.isPosture;

        }

        /*
         * Gibt die Anzahl der registrierten Hold-Positionen zurück
         */
        public int GetHoldCount()
        {
            return this.holdPositions.Count;
        }

        /* 
         * Bestimmt, ob der übergebene Punkt innerhalb der Handfläche liegt
         */
        public bool IsInsideHand(Point testPoint)
        {
            Console.WriteLine("MyPostureRecognizer_IsInsideHand() -> Pos:{0},{1}", testPoint.X, testPoint.Y);
            bool result = false;
            int j = this.posturePositions.Count() - 1;
            for (int i = 0; i < this.posturePositions.Count(); i++)
            {
                if (this.posturePositions[i].GetPoint().Y < testPoint.Y && this.posturePositions[j].GetPoint().Y >= testPoint.Y || this.posturePositions[j].GetPoint().Y < testPoint.Y && this.posturePositions[i].GetPoint().Y >= testPoint.Y)
                {
                    if (this.posturePositions[i].GetPoint().X + (testPoint.Y - this.posturePositions[i].GetPoint().Y) / (this.posturePositions[j].GetPoint().Y - this.posturePositions[i].GetPoint().Y) * (this.posturePositions[j].GetPoint().X - this.posturePositions[i].GetPoint().X) < testPoint.X)
                    {
                        result = !result;
                    }
                }
                j = i;
            }
            return result;
        }
    }

    /*
     * Hilfsklasse, die es ermöglicht Punkten einem Punkt eine Touch-ID und den aktuellen Zeitpunkt zuzuweisene
     */
    class TimedPoint
    {
        private int touchID;    //Eindeutige ID des Fingers (wichtig zum Entfernen des Touches bei TouchUp)
        private Point p;        //Position des Touches
        private double time;    //Aktuelle Zeit in msec

        public TimedPoint(int id, Point p, double time)
        {
            this.touchID = id;
            this.p = p;
            this.time = time;
        }

        public int GetID()
        {
            return this.touchID;
        }

        public Point GetPoint()
        {
            return this.p;
        }

        public double GetTime()
        {
            return time;
        }
    }
}
