﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;


namespace MuSeCo_ViewModel.Gesture
{
    /*
     * Klasse wird im MainSurfaceView stetig mit TouchPunkten versorgt. 
     * Erkennt über die Check-Methode, ob eine Geste ausgeführt wurde.
     */
    class MyGestureRecognizer
    {
        private Point centerPosition;       //Mittelpunkt des Zielgebiets         
        private List<Point> points;         //Liste mit allen Zwischen-Punkten während der Geste        
        private Vector referenceVector;     //Referenzvektor, um den Kreis-Fortschritt zu berechnen
        private Circle circleProgress;      //Aktueller Fortschritt der Kreisgeste
        private double lastAngle;           //Winkel aus vorherigem Schritt zum Vergleich
        private bool isClockwise = true;    //Wird die Kreisgeste im Uhrzeigersinn ausgeführt?

        /*
         * Konstruktor setzt center-Parameter als Zentrum für die Erkennung
         */
        public MyGestureRecognizer(Point center)
        {
            this.SetCenter(center);
        }

        /*
         * Startpunkt wird gesetzt, um neue Gesten-Erkennung zu starten.
         * Beinhaltet das Löschen der bisherigen Punkte.
         */
        public void SetCenter(Point center)
        {
            Console.WriteLine("MyGestureRecognizer_SetCenter() -> Pos:{0},{1}", center.X, center.Y);

            this.centerPosition = center;
            this.Reset();
        }

        /*
         * Gesten-Erkennung wird zurückgesetzt
         */
        public void Reset()
        {
            this.points = new List<Point>();
            this.circleProgress = Circle.EMPTY;
            this.lastAngle = 0;
        }

        /*
         * Neuer Touch-Punkt wird hinzugefügt
         */
        public void AddPoint(Point p)
        {
            this.points.Add(p);
            if (this.points.Count == 1)
                this.referenceVector = new Vector(p.X - this.centerPosition.X, p.Y - this.centerPosition.Y);
        }

        /*
         * Gibt die Anzahl der Messpunkte zurück
         */
        public int GetPointCount()
        {
            return this.points.Count;
        }

        /*
         * Überprüft, ob aktuelle TouchPunkte bereits eine Geste bilden
         */
        public MyGestureSet CheckGesture()
        {
            Console.WriteLine("MyGestureRecognizer_CheckGesture()");

            if (this.points.Count <= 1)
            {
                this.lastAngle = 0;
                return MyGestureSet.NONE;
            }

            //if (this.points.Count >= 6)
            //{
            //    //MessageBox.Show("keine Gester wird erkkannt");
            //    return MyGestureSet.NONE;
            //}

            //Berechne den Winkel zum Referenzvektor
            Vector currentVector = new Vector(this.points.ElementAt(this.points.Count - 1).X - this.centerPosition.X, this.points.ElementAt(this.points.Count - 1).Y - this.centerPosition.Y);
            double currentAngle = Vector.AngleBetween(this.referenceVector, currentVector);

            //Prüfe anfangs auf Drehrichtung
            if (this.points.Count == 2 && currentAngle > 0)
                this.isClockwise = true;
            else if (this.points.Count == 2)
                this.isClockwise = false;

            //Prüfe Fortschritt der Kreisgeste
            if (isClockwise)
            {
                if (this.lastAngle > 90 && currentAngle < -90)
                    this.circleProgress = Circle.HALF;
                else if (currentAngle > 0 && this.lastAngle < 0 && this.circleProgress == Circle.HALF)
                    this.circleProgress = Circle.FULL;
            }
            else
            {
                if (currentAngle > 90 && this.lastAngle < -90)
                    this.circleProgress = Circle.HALF;
                else if (currentAngle < 0 && this.lastAngle > 0 && this.circleProgress == Circle.HALF)
                    this.circleProgress = Circle.FULL;
            }

            this.lastAngle = currentAngle;

            if (this.circleProgress == Circle.FULL)
            {
                this.circleProgress = Circle.EMPTY;
                this.points = new List<Point>();
                return MyGestureSet.CIRCLE;
            }
            else
                return MyGestureSet.NONE;
        }
    }

    /*
     * Hilfs-Enum zur Dokumentierung des Kreis-Fortschritts
     */
    public enum Circle
    {
        EMPTY = 0,
        HALF = 1,
        FULL = 2
    }
}
