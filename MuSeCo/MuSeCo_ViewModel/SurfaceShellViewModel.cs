﻿using System;
using System.Collections.ObjectModel;
using MuSeCo_Model;

using Microsoft.Surface.Presentation.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using System.IO;
using System.Resources;
using System.Globalization;
using System.Collections;
using Microsoft.Surface.Presentation.Input;
using System.Windows.Media.Animation;
using GalaSoft.MvvmLight.Command;
using MuSeCo_ViewModel.Gesture;
using Blake.NUI.WPF.Gestures;
using System.Windows.Forms;

namespace MuSeCo_ViewModel
{
    public class SurfaceShellViewModel : ViewModelBase
    {




        #region private fields
        private ObservableCollection<ISearchable> _searchableCollection;
        private ObservableCollection<ScatterViewItem> _searchablescatterviewitems;
        private RotateTransform rt = new RotateTransform();
        private Viewbox vdummy = new Viewbox();

        #endregion

        #region public field

        public ObservableCollection<ISearchable> SearchableCollection
        {
            get { return _searchableCollection; }
            set 
            {
                _searchableCollection = value;
                RaisePropertyChanged("SearchableCollection");
            }
        }


        public ObservableCollection<ScatterViewItem> searchablescatterviewitems
        {
            get { return _searchablescatterviewitems; }
            set
            {
                _searchablescatterviewitems = value;
                RaisePropertyChanged("searchablescatterviewitems");
            }
        }
        #endregion

        //zum Verabrieten der Touch-Events aus der HauptView für das Tidy-UP-Widget
        public RelayCommand<TouchEventArgs> TouchDownCommand
        {
            get;
            private set;
        }
        public RelayCommand<TouchEventArgs> TouchMoveCommand
        {
            get;
            private set;
        }
        public RelayCommand<TouchEventArgs> TouchUpCommand
        {
            get;
            private set;
        }

        #region Tidy-up-Widget Variablen
        ScatterView _view;
        //Zielgebiet
        private ScatterViewItem targetArea; //Kreis-Markierung für Zielbereich
        //Gesture-Recognizer        
        private MyGestureRecognizer gestureRecognizer;  //Erkennung der Gesten

        //Gesture-Handler
        private MyGestureHandler gestureHandler; //GestureHandler behandelt eintreffende Gesten

        //Posture-Recognizer
        private MyPostureRecognizer postureRecognizer;  //Erkennung der Posture (Initialisierung der Geste)
        private bool isPosture;                         //Wird die Posture ausgeführt?        
        private bool isGestureReady;                    //Kann Gesten-Erkennung gestartet werden? (nachdem Zielgebiet gesetzt wurde)
        #endregion


        #region ctor
        public SurfaceShellViewModel(ScatterView view)
        {
            _view = view;

            // Collection dient zur Verarbeitung der Itempages durch die Geste vov Fabian Mense
            _searchablescatterviewitems = new ObservableCollection<ScatterViewItem>();

            // Stellt die ItemSource dar
            _searchableCollection = new ObservableCollection<ISearchable>();

            // Fügt das Zentralmenü bei der Initaliserung der Anwendung hinzu
            _searchableCollection.Add(new RadialMenuViewModel());

            //dummyklasse wird hinzugefügt
            _searchableCollection.Add(new TaktCompareViewModel());



            // Empfängt die Events der anderen Viewmodels
            EventAggregator.GetEvent<AddSourceEvent>().Subscribe(searchString => addSource(searchString));
            EventAggregator.GetEvent<RemoveEvent>().Subscribe(searchable => Remove(searchable));
            EventAggregator.GetEvent<AddEvent>().Subscribe(searchable => Insert(searchable));
            EventAggregator.GetEvent<AddScatterItemEvent>().Subscribe(searchable => Insert2(searchable));
            EventAggregator.GetEvent <RotationEvent>().Subscribe(searchable => rotate(searchable));
            EventAggregator.GetEvent<AddNoteEvent>().Subscribe(searchString => addNote(searchString));


            #region Tidy-Up-Widget Logik
            //Widget-Zielgebiet
            Ellipse circleArea = new Ellipse { Width = 250, Height = 250, Stroke = Brushes.DodgerBlue, StrokeThickness = 3 };
            this.targetArea = new ScatterViewItem();
            this.targetArea.Content = circleArea;
            this.targetArea.Center = new Point(0, 0);
            this.targetArea.Width = 250.0;
            this.targetArea.Height = 250.0;
            ImageBrush widgetBackground = new ImageBrush();
            //BitmapImage widgetBackgroundImage = new BitmapImage(new Uri(@"C:\Users\Kubus\Downloads\Fabian Mense\Fabian Mense\implementierung\TidyUpWidget\Resources\WindowBackground.jpg", UriKind.RelativeOrAbsolute));
            //widgetBackground.ImageSource = widgetBackgroundImage;
            widgetBackground.Opacity = 0.1;
            this.targetArea.Background = widgetBackground;
            this.targetArea.Visibility = Visibility.Hidden;
            this.targetArea.Orientation = 0.0;
            this.targetArea.IsManipulationEnabled = false;
            //this.targetArea.TouchUp += TouchUpCommand; //Zum Ausblenden des Kreises und Einblenden der Library
            //this.targetArea.PreviewTouchUp += this.scatterViewItem_TouchUp;
            //_searchableCollection.Add(this.targetArea);
            //this.testview.Items.Add(this.targetArea);

            //Gesture-Recognizer-Code
            this.gestureRecognizer = new MyGestureRecognizer(this.targetArea.Center);

            //Gesture-Handler
            this.gestureHandler = new MyGestureHandler(view, _searchablescatterviewitems, this.targetArea);

            //Posture-Recognizer-Code
            this.postureRecognizer = new MyPostureRecognizer();

            //Tap and Hold

            Events.RegisterGestureEventSupport(view);
            Events.AddHoldGestureHandler(view, PanelLeft_TapGesture);


            TouchDownCommand = new RelayCommand<TouchEventArgs>(e =>
            {

               

                //tmodel.OpacityStateTarget = "Hidden";
                Point currentTouch = e.GetTouchPoint(view).Position;
                Console.WriteLine("MainSurfaceWindow_TouchDown -> {0};{1}", currentTouch.X, currentTouch.Y);
                //MessageBox.Show("MainSurfaceWindow_TouchDown -> {0};{1}"+ currentTouch.X +  currentTouch.Y);
                if (this.isPosture && !(this.isGestureReady)) //Zielgebiet setzen
                {


                    this.targetArea.Center = currentTouch;
                    this.targetArea.Visibility = Visibility.Visible;
                    this.gestureRecognizer.SetCenter(currentTouch);
                    this.isGestureReady = true;
                }

            });

            TouchMoveCommand = new RelayCommand<TouchEventArgs>(e =>
            {

                Point currentTouch = e.GetTouchPoint(_view).Position;
                Console.WriteLine("MainSurfaceWindow_TouchMove -> {0};{1}", currentTouch.X, currentTouch.Y);

                if (this.isPosture && this.isGestureReady) //Neue Gestenerkennung beginnen
                {

                    this.gestureRecognizer.AddPoint(currentTouch);
                    MyGestureSet gs = this.gestureRecognizer.CheckGesture();
                    //MessageBox.Show(gs + "");
                    if (gs != MyGestureSet.NONE)
                    {
                        this.gestureHandler.HandleGesture(gs, true, this.targetArea.Center);
                    }
                }

            });

            TouchUpCommand = new RelayCommand<TouchEventArgs>(e =>
            {

                Point currentTouch = e.GetTouchPoint(view).Position;
                Console.WriteLine("MainSurfaceWindow_TouchUp() -> POS:{0},{1} ID:{2}", currentTouch.X, currentTouch.Y, e.TouchDevice.Id);

                //Posture-Recognizer-Code
                this.postureRecognizer.RemoveHoldPosition(e.TouchDevice.Id);
                this.isPosture = this.postureRecognizer.IsPosturePerformed();
                //MessageBox.Show("test");
                Console.WriteLine("MainSurfaceWindow_TouchUp() -> Posture:{0}", this.isPosture);

                if (this.isPosture)
                {
                    if (postureRecognizer.IsInsideHand(currentTouch))
                    {
                        this.gestureHandler.HandleGesture(MyGestureSet.FLICK_LEFT, true, currentTouch);
                        this.gestureRecognizer.Reset();
                        this.isGestureReady = false;
                        this.postureRecognizer.Reset();
                        this.isPosture = this.postureRecognizer.IsPosturePerformed();
                    }
                }
                else
                {
                    this.targetArea.Visibility = Visibility.Hidden;
                    this.isGestureReady = false;
                }
                this.gestureRecognizer.Reset();

            });

            #endregion

        }
        #endregion


        #region zwei MEthoden für das Tidy-UP-Wiget
        private void scatterView_HoldGesture(object sender, TouchEventArgs e)
        {
            Point p = e.TouchDevice.GetPosition(_view);
            Console.WriteLine("MainSurfaceWindow_Hold() -> Hold an Position {0},{1} mit fingerID={2}", p.X, p.Y, e.TouchDevice.Id);
            //MessageBox.Show("tetst");

            if (!this.isPosture)
            {
                this.postureRecognizer.AddHoldPosition(e.TouchDevice.Id, p);
                this.isPosture = this.postureRecognizer.IsPosturePerformed();

                if (this.isPosture)
                {

                }
                //this.ShowPostureTooltip();
            }
        }

        private void PanelLeft_TapGesture(object sender, Blake.NUI.WPF.Gestures.GestureEventArgs e)
        {

            Point p = e.TouchDevice.GetPosition(_view);
            Console.WriteLine("MainSurfaceWindow_Hold() -> Hold an Position {0},{1} mit fingerID={2}", p.X, p.Y, e.TouchDevice.Id);
            //MessageBox.Show("tetst");

            if (!this.isPosture)
            {
                this.postureRecognizer.AddHoldPosition(e.TouchDevice.Id, p);
                this.isPosture = this.postureRecognizer.IsPosturePerformed();

                if (this.isPosture)
                {

                }
                //this.ShowPostureTooltip();
            }
        }
        #endregion

        #region private methods

        private void addSource(string searchString)
        {
            SearchableCollection.Add(new SearchResultViewModel());
        }

        private void addNote(SurfaceNoteViewModel mp)
        {
            SearchableCollection.Add(mp);
        }

        private void addNote(string searchString)
        {
            SearchableCollection.Add(new SurfaceNoteViewModel(searchString));
        }

        private void Remove(ISearchable searchable)
        {
            SearchableCollection.Remove(searchable);
        }

        private void Insert(ISearchable searchable)
        {
            SearchableCollection.Add(searchable);
        }

        private void Insert2(ScatterViewItem searchable)
        {
            _searchablescatterviewitems.Add(searchable);
        }

        private void rotate(string searchString)
        {
            if (rt.Angle == 0)
            {
                rt.Angle = 180;
                _view.LayoutTransform = rt;

            }
            else
            {
                rt.Angle = 0;
                _view.LayoutTransform = rt;

            }

        }

        #endregion




    }
}
