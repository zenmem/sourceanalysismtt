﻿using MuSeCo.Model;
using MuSeCo_Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Command;
using MuSeCo_ViewModel.Controls;
using System.Diagnostics;
using System.Management;
using Blake.NUI.WPF.Gestures;
using Microsoft.Practices.Prism.Commands;
using System.Collections.ObjectModel;

namespace MuSeCo_ViewModel
{
    public class TaktCompareViewModel : ViewModelBase, ISearchable, INotifyPropertyChanged
    {
        #region Deklarationen

        private List<string> _listcroppedImagespath;
        private List<string> _listcroppedImagespath2;

        private double _width = 1000;


        private string _numberOfItempagesLeft;


        private ObservableCollection<Item> _items = new ObservableCollection<Item>();


   
        private string _gText = "";
        private string _giveSource;


        private int _giveH1 = 300;


        //TODO This is only a test mathod to syncronize the itemPages in the selected items.
        // hier ist Seiten nummer.
        private int _currentPage = 0;

        private string _defaultPage = "..\\Images\\Drag and drop.png";


        private DelegateCommand<string> _addNoteCommand;



        #endregion
        #region Konstruktor
        public TaktCompareViewModel()
        {


            _addNoteCommand = new DelegateCommand<string>(SearchExecute2);

            #region Lange Tap Geste und double Tab Geste regestrieren

            //leftStackviewbox1 = viewbox;
           

            //Events.AddHoldGestureHandler(viewbox, MeasureContetextMenu1Event);
            //Events.AddDoubleTapGestureHandler(viewbox, ZoomMeasure1Event);

          
            #endregion


            _listcroppedImagespath = new List<string>();
            _listcroppedImagespath2 = new List<string>();

            SetSourceItempage = "..\\Images\\here to process.png";

            numberOfItempagesLeft = "0 / 0";

        }
        #endregion

      

        #region Button Commands

        public ICommand Button_ClickL
        {
            get
            {
                return new RelayCommand(() =>
                {
                    if (CurrentPage != 0)
                        CurrentPage--;

                    // if(_ItemPageCounter <= 1)
                    //  updatePanel(_directory, _ItemPageCounter);
                    // else
                    //{
                    //   _ItemPageCounter--;
                    // updatePanel(_directory, _ItemPageCounter);
                    //}
                });
            }
        }

        public ICommand Button_ClickR
        {
            get
            {
                return new RelayCommand(() =>
                {
                    CurrentPage++;
                    //DirectoryInfo d = new DirectoryInfo(_directory);
                    //FileInfo[] Files = d.GetFiles("*.jpg");
                    //if (_ItemPageCounter >= Files.Length)
                    //    updatePanel(_directory, _ItemPageCounter);
                    //else
                    //{
                    //    _ItemPageCounter++;
                    //    updatePanel(_directory, _ItemPageCounter);
                    //}



                });
            }
        }
        #endregion

        #region Methoden



        private void updatePanel(string path, int pagenumber)
        {
            DirectoryInfo d = new DirectoryInfo(path);
            FileInfo[] Files = d.GetFiles("*.jpg");
            //ItemDirectory = Files[pagenumber - 1].DirectoryName;
            SetSourceItempage = Files[pagenumber-1].DirectoryName + "\\" + Files[pagenumber-1].Name;
            
        }

      
        #endregion



        public ICommand AddNoteCommand
        {
            get
            {
                return _addNoteCommand;
            }
        }

        private void SearchExecute2(string searchString)
        {
            EventAggregator.GetEvent<AddNoteEvent>().Publish("WOW das könnte was werden");
        }

        #region Public Properties für die  View

        public int CurrentPage
        {
            get { return _currentPage; }
            set
            {
                _currentPage = value;
                RaisePropertyChanged("CurrentPage");
            }
        }

        public double Width
        {
            get { return _width; }
            set
            {
                _width = value;
                RaisePropertyChanged("Width");
            }
        }

        public string DefaultPage
        {
            get { return _defaultPage; }
        }
        public ObservableCollection<Item> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
                RaisePropertyChanged("Items");
            }
        }

     




     



        public string numberOfItempagesLeft
        {
            get
            {
                return _numberOfItempagesLeft;
            }
            set
            {
                _numberOfItempagesLeft = value;
                RaisePropertyChanged("numberOfItempagesLeft");

            }
        }
      

        public string gText
        {
            get
            {
                return _gText;
            }
            set
            {
                _gText = value;
                RaisePropertyChanged("gText");

            }
        }


 
   

      
       
        public string SetSourceItempage
        {
            get
            {
                return _giveSource;
            }
            set
            {
                _giveSource = value;
                RaisePropertyChanged("SetSourceItempage");

            }
        }

    



        public int HeightLeftCroppedItempage1
        {
            get
            {
                return _giveH1;
            }
            set
            {
                _giveH1 = value;
                RaisePropertyChanged("HeightLeftCroppedItempage1");

            }
        }

  

    
        #endregion



        #region Funktionen für das Zoomen
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZoomMeasure1Event(object sender, Blake.NUI.WPF.Gestures.GestureEventArgs e)
        {

           /* if (counting < 3)
            {
                HeightLeftCroppedItempage1 = HeightLeftCroppedItempage1 + 50;
                counting++;
            }
            else
            {
                HeightLeftCroppedItempage1 = HeightLeftCroppedItempage1 - 100;
                counting = 1;
            }
            e.Handled = true;*/

        }
        #endregion

        #region Kontextmenü-Events
        private void MeasureContetextMenu1Event(object sender, Blake.NUI.WPF.Gestures.GestureEventArgs e)
        {
            //ContextMenu cm = leftStackviewbox1.FindResource("cmMeasureLeftPanelPrior") as ContextMenu;
            //cm.PlacementTarget = sender as Button;
            //cm.IsOpen = true;
            e.Handled = true;
            //MessageBox.Show("This is a Test");
        }

       

   
     

        #endregion


    }
}


