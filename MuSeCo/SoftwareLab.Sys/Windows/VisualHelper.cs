﻿using System.Windows;
using System.Windows.Media;

namespace SoftwareLab.Sys.Windows
{
    public static class VisualHelper
    {
        public static T GetVisualAncestor<T>(DependencyObject descendent) where T : class
        {
            DependencyObject reference = descendent;
            T local = default(T);
            while ((reference != null) && ((local = reference as T) == null))
            {
                reference = VisualTreeHelper.GetParent(reference);
            }
            return local;
        }
    }
}
