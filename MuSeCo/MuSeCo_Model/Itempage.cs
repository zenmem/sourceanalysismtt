﻿using SoftwareLab.Sys.ComponentModel;
using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace MuSeCo_Model
{
    public class ItemPage : ObservableObject, ISearchable
    {
        #region private properties
        private string _thumbnailUrl;
        private string _url;
        private string _status;
        private string _label;
        private double _height;
        private double _width;
        private int _id;
        private List<Measure> _measures;
   
        private Item _parent;

        #endregion

        #region public properties
        public string ThumbnailUrl
        {
            get { return _thumbnailUrl; }
            set
            {
                _thumbnailUrl = value;
                RaisePropertyChanged("ThumbnailUrl");
            }
        }

        public string Url
        {
            get { return _url; }
            set 
            { 
                _url = value;
                BitmapImage img = new BitmapImage(new Uri(@_url));
                _width = img.Width;
                _height = img.Height;
                RaisePropertyChanged("Url");
                RaisePropertyChanged("Width");
                RaisePropertyChanged("Height");
            }
        }

        public double Width
        {
            get { return _width; }
        }

        public double Height
        {
            get { return _height; }
        }
        #endregion


        public List<Measure> Measures
        {
            get
            {
                return _measures;
            }
            set
            {
                _measures = value;

            }
        }

        public Item Parent
        {
            get
            {
                return _parent;
            }
            set
            {
                _parent = value;

            }
        }

        public string Label
        {
            get
            {
                return _label;
            }
            set
            {
                _label = value;

            }
        }

        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;

            }
        }


        #region ctor
        public ItemPage(Item parent)
        {
            _parent = parent;
            _measures = new List<Measure>();
           
        }
        #endregion
    }
}
