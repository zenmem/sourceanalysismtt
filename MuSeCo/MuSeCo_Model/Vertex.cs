﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MuSeCo_Model
{
    public class Vertex
    {
        private int _id;
        private int _x;
        private int _y;

        public Vertex()
        {

        }


        public int X
        {
            get
            {
                return _x;
            }
            set
            {
                _x = value;

            }
        }


        public int Y
        {
            get
            {
                return _y;
            }
            set
            {
                _y = value;

            }
        }
    }
}
