﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MuSeCo_Model
{
    class Zone
    {
        private int _id;
        private int _ordinal;
        private bool _visible;
        private bool _excluded;
        private bool _locked;
        private bool _rectangle;

        private List<Vertex> _vertices;

        //ToDo
        public Zone()
        {

        }


        public int Id
        {
            get { return _id; }
            set { _id = value;  }
        }

        public int Ordinal
        {
            get { return _ordinal; }
            set { _ordinal = value; }
        }

        public bool Excluded
        {
            get { return _excluded; }
            set { _excluded = value; }
        }

        public bool Visible
        {
            get { return _visible; }
            set { _visible = value; }
        }

        public bool Locked
        {
            get { return _locked; }
            set { _locked = value; }
        }

        public bool Rectangle
        {
            get { return _rectangle; }
            set { _rectangle = value; }
        }

        public List<Vertex> Vertices
        {
            get { return _vertices; }
        }
    }
}
