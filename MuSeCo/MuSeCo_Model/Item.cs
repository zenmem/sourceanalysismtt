﻿using MuSeCo_Model;
using SoftwareLab.Sys.ComponentModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MuSeCo_Model
{
    public class Item: ObservableObject
    {


        private ObservableCollection<ItemPage> _pages;
        private string _label;
        private int _id;
        private string _status;

        public Item()
        {
            _pages = new ObservableCollection<ItemPage>();
            _label = "";
            _id = 0;
            _status = "";
        }

        public string Label
        {
            get
            {
                return _label;
            }
            set
            {
                _label = value;
            }
        }


        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;

            }
        }

        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;

            }
        }

        public ObservableCollection<ItemPage> Pages
        {
            get
            {
                return _pages;
            }
        }
    }
}
